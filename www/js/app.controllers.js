angular.module('app.controllers', [])
  .controller('AppController', function ($scope, $ionicSideMenuDelegate) {

    $scope.toggleLeft = function () {
      $ionicSideMenuDelegate.toggleLeft();
    };
    $scope.login = false;
    $scope.getLogin = function(){
      return $scope.login;
    };
    $scope.setLogin = function(){
        $scope.login = !$scope.login;
    }
  })
  .controller('homeCtrl', function ($scope, $ionicSideMenuDelegate, $ionicPopover, searchResults, autocompleteSearch, $q, $timeout) {
    // console.log(searchResults.searchArtist());
    $scope.getMatches = function(searchText) {
            var deferred = $q.defer();

            $timeout(function() {
                var states = getKeywords().filter(function(state) {
                    return (state.name.toUpperCase().indexOf(searchText.toUpperCase()) !== -1 || state.abbreviation.toUpperCase().indexOf(searchText.toUpperCase()) !== -1);
                });
                deferred.resolve(states);
                console.log('print getMatches is active');
            }, 100);

            return deferred.promise;
        };

        function getKeywords() {
          console.log('print getState is active');
    return [{
        "name": "Alabama",
        "abbreviation": "AL"
    }, {
        "name": "Alaska",
        "abbreviation": "AK"
    }, {
        "name": "American Samoa",
        "abbreviation": "AS"
    }, {
        "name": "Arizona",
        "abbreviation": "AZ"
    }, {
        "name": "Arkansas",
        "abbreviation": "AR"
    }, {
        "name": "California",
        "abbreviation": "CA"
    }, {
        "name": "Colorado",
        "abbreviation": "CO"
    }, {
        "name": "Connecticut",
        "abbreviation": "CT"
    }, {
        "name": "Delaware",
        "abbreviation": "DE"
    }, {
        "name": "District Of Columbia",
        "abbreviation": "DC"
    }, {
        "name": "Federated States Of Micronesia",
        "abbreviation": "FM"
    }, {
        "name": "Florida",
        "abbreviation": "FL"
    }, {
        "name": "Georgia",
        "abbreviation": "GA"
    }, {
        "name": "Guam",
        "abbreviation": "GU"
    }, {
        "name": "Hawaii",
        "abbreviation": "HI"
    }, {
        "name": "Idaho",
        "abbreviation": "ID"
    }, {
        "name": "Illinois",
        "abbreviation": "IL"
    }, {
        "name": "Indiana",
        "abbreviation": "IN"
    }, {
        "name": "Iowa",
        "abbreviation": "IA"
    }, {
        "name": "Kansas",
        "abbreviation": "KS"
    }, {
        "name": "Kentucky",
        "abbreviation": "KY"
    }, {
        "name": "Louisiana",
        "abbreviation": "LA"
    }, {
        "name": "Maine",
        "abbreviation": "ME"
    }, {
        "name": "Marshall Islands",
        "abbreviation": "MH"
    }, {
        "name": "Maryland",
        "abbreviation": "MD"
    }, {
        "name": "Massachusetts",
        "abbreviation": "MA"
    }, {
        "name": "Michigan",
        "abbreviation": "MI"
    }, {
        "name": "Minnesota",
        "abbreviation": "MN"
    }, {
        "name": "Mississippi",
        "abbreviation": "MS"
    }, {
        "name": "Missouri",
        "abbreviation": "MO"
    }, {
        "name": "Montana",
        "abbreviation": "MT"
    }, {
        "name": "Nebraska",
        "abbreviation": "NE"
    }, {
        "name": "Nevada",
        "abbreviation": "NV"
    }, {
        "name": "New Hampshire",
        "abbreviation": "NH"
    }, {
        "name": "New Jersey",
        "abbreviation": "NJ"
    }, {
        "name": "New Mexico",
        "abbreviation": "NM"
    }, {
        "name": "New York",
        "abbreviation": "NY"
    }, {
        "name": "North Carolina",
        "abbreviation": "NC"
    }, {
        "name": "North Dakota",
        "abbreviation": "ND"
    }, {
        "name": "Northern Mariana Islands",
        "abbreviation": "MP"
    }, {
        "name": "Ohio",
        "abbreviation": "OH"
    }, {
        "name": "Oklahoma",
        "abbreviation": "OK"
    }, {
        "name": "Oregon",
        "abbreviation": "OR"
    }, {
        "name": "Palau",
        "abbreviation": "PW"
    }, {
        "name": "Pennsylvania",
        "abbreviation": "PA"
    }, {
        "name": "Puerto Rico",
        "abbreviation": "PR"
    }, {
        "name": "Rhode Island",
        "abbreviation": "RI"
    }, {
        "name": "South Carolina",
        "abbreviation": "SC"
    }, {
        "name": "South Dakota",
        "abbreviation": "SD"
    }, {
        "name": "Tennessee",
        "abbreviation": "TN"
    }, {
        "name": "Texas",
        "abbreviation": "TX"
    }, {
        "name": "Utah",
        "abbreviation": "UT"
    }, {
        "name": "Vermont",
        "abbreviation": "VT"
    }, {
        "name": "Virgin Islands",
        "abbreviation": "VI"
    }, {
        "name": "Virginia",
        "abbreviation": "VA"
    }, {
        "name": "Washington",
        "abbreviation": "WA"
    }, {
        "name": "West Virginia",
        "abbreviation": "WV"
    }, {
        "name": "Wisconsin",
        "abbreviation": "WI"
    }, {
        "name": "Wyoming",
        "abbreviation": "WY"
    }];
}

    $scope.artistData = searchResults.searchArtist();
    $scope.musicData = searchResults.searchMusic();
    $scope.artistRelatedData = searchResults.searchArtistRelated();
    $scope.suggestedData = searchResults.searchSuggest();
    $scope.autocompleteSearchArtist = autocompleteSearch.searchArtist();
    $scope.autocompleteSearchMusic = autocompleteSearch.searchMusic();

    $scope.autocomplete = false;
    $scope.setAutocomplete = function () {
      $scope.autocomplete = !$scope.autocomplete;
    };
    $scope.select = 1;
    $scope.changeSelect = function(num){
      $scope.select = num;
    };
    $scope.getSelect = function(num){
      if ($scope.select == num) {
        return true;
      }else {
        return false;
      }
    };

    $ionicPopover.fromTemplateUrl('../modules/home/addtoplaylist.popover.html', {
        scope: $scope
     }).then(function(popover) {
        $scope.popover = popover;
     });

   $scope.openPopover = function($event) {
      $scope.popover.show($event);
   };

   $scope.closePopover = function() {
      $scope.popover.hide();
   };

   //Cleanup the popover when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.popover.remove();
   });

   // Execute action on hide popover
   $scope.$on('popover.hidden', function() {
      // Execute action
   });

   // Execute action on remove popover
   $scope.$on('popover.removed', function() {
      // Execute action
   });

  })
  .controller('SongCtrl', function ($scope, $ionicSideMenuDelegate, $stateParams, $http, $timeout, $ionicLoading, Keys) {
    $scope.keyup = 0;
    $scope.increase = function () {
      $scope.keyup++;
    };
    $scope.decrease = function () {
      $scope.keyup--;
      if($scope.keyup < 0)
        $scope.keyup = 11;
    };
    $scope.getChord = function (chord) {
      console.log("Chord:"+chord);
      if (typeof (chord) != 'string') {
        //console.log(typeof (chord));
        var key1 = (chord/1000).toFixed(0);
        var key2 = ((chord%1000)/100).toFixed(0);
        var addonKey = chord%100;
        console.log("Chord:"+chord+" key1="+key1+" keyup="+$scope.keyup);
        var mainKey = parseInt(key1) + parseInt($scope.keyup);
        if(mainKey < 0) mainKey = 11;
        else if(mainKey > 11 ) mainKey = mainKey%12;
        console.log('mainKey'+":"+mainKey);
        return Keys.get(mainKey,key2,addonKey);
      } else {
        //console.log("String");
        return chord;
      }

    };

    $scope.song = 'Song name';
              $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
          });

      $scope.music = [];
      $scope.key = 0;
      $scope.melody = {
        1001 : { melody:'A'}
      };
      console.log($scope.melody);

    $http.get('https://chord-test.firebaseio.com/Music_lists/40000/.json')
    .then(function(response) {
            // service.music = response;
          $timeout(function () {
    $ionicLoading.hide();
    $scope.music = response.data;
            console.log(response);
  }, 1);

//            $scope.music = response.data;
//            console.log(response);
    });
  })
    .controller('SongsCtrl', function ($scope, $ionicSideMenuDelegate, $stateParams, $http, $timeout, $ionicLoading, Keys) {
        $scope.keyup = 0;
    $scope.increase = function () {
      $scope.keyup++;
    };
    $scope.decrease = function () {
      $scope.keyup--;
      if($scope.keyup < 0)
        $scope.keyup = 11;
    };
    $scope.getChord = function (chord) {
      console.log("Chord:"+chord);
      if (typeof (chord) != 'string') {
        //console.log(typeof (chord));
        var key1 = (chord/1000).toFixed(0);
        var key2 = ((chord%1000)/100).toFixed(0);
        var addonKey = chord%100;
        console.log("Chord:"+chord+" key1="+key1+" keyup="+$scope.keyup);
        var mainKey = parseInt(key1) + parseInt($scope.keyup);
        if(mainKey < 0) mainKey = 11;
        else if(mainKey > 11 ) mainKey = mainKey%12;
        console.log('mainKey'+":"+mainKey);
        return Keys.get(mainKey,key2,addonKey);
      } else {
        //console.log("String");
        return chord;
      }

    };
    $scope.song = 'Song name';
              $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
          });

      $scope.music = [];

    $http.get('https://chord-test.firebaseio.com/Music_lists/40000/.json')
    .then(function(response) {
            // service.music = response;

          $timeout(function () {
    $ionicLoading.hide();
    $scope.music = response.data;
            console.log(response);
  }, 1);

//            $scope.music = response.data;
//            console.log(response);
    });
  })
  .controller('PlaylistsCtrl', function ($scope, $ionicSideMenuDelegate, $ionicPopup, $timeout) {
    $scope.showPopup = function() {
   $scope.playlist = {}

   // An elaborate, custom popup
   var myPopup = $ionicPopup.show({
     template: '<input type="text" ng-model="playlist.name">',
     title: 'Create Playlist',
     subTitle: 'Please enter your playlist name',
     scope: $scope,
     buttons: [
       { text: 'Cancel' },
       {
         text: '<b>Save</b>',
         type: 'button-dark',
         onTap: function(e) {
           if (!$scope.playlist.name) {
             //don't allow the user to close unless he enters wifi password
             e.preventDefault();
           } else {
             return $scope.playlist.name;
           }
         }
       },
     ]
   });
   myPopup.then(function(res) {
     console.log('Tapped!', res);
   });
   $timeout(function() {
      myPopup.close(); //close the popup after 3 seconds for some reason
   }, 10000);
  };

  })
  .controller('PlaylistCtrl', function ($scope, $ionicSideMenuDelegate, $ionicPopup, $timeout, $ionicModal) {
    $ionicModal.fromTemplateUrl('../modules/playlist/modal.addsong2playlist.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.autocomplete = false;
  $scope.setAutocomplete = function () {
    $scope.autocomplete = !$scope.autocomplete;
  };
  $scope.select = 1;
  $scope.changeSelect = function(num){
    $scope.select = num;
  };
  $scope.getSelect = function(num){
    if ($scope.select == num) {
      return true;
    }else {
      return false;
    }
  };

  })
