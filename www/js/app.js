// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic',
  // 'ionic.service.core',
  // 'ionic.service.analytics',
  'app.controllers',
  'app.services',
  'app.factorys',
  'app.directives',
  'ngCordova',
  'ngMaterial',
  'ngAnimate',
  'ngAria'
])
  .constant('app_version', '1.0.0')
  .constant('$ionicLoadingConfig', {
    noBackdrop: false,
    template: '<ion-spinner icon="android"></ion-spinner><br>Loading...'
  })
  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppController'
      })

      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'modules/home/home.html',
            controller: 'homeCtrl'
          }
        }
      })
      .state('app.playlists', {
        url: '/playlists',
        views: {
          'menuContent': {
            templateUrl: 'modules/playlists/playlists.html',
            controller: 'PlaylistsCtrl'
          }
        }
      })
      .state('app.playlist', {
        url: '/playlists/playlist/:id',
        views: {
          'menuContent': {
            templateUrl: 'modules/playlist/playlist.html',
            controller: 'PlaylistCtrl'
          }
        }
      })
      .state('app.song', {
        url: '/song',
        views: {
          'menuContent': {
            templateUrl: 'modules/song/song.html',
            controller: 'SongCtrl'
          }
        }
      })
      .state('app.songs', {
        url: '/playlists/playlist/:playlistId/songs/:songId',
        views: {
          'menuContent': {
            templateUrl: 'modules/songs/songs.html',
            controller: 'SongsCtrl'
          }
        }
      })
    $urlRouterProvider.otherwise('/app/home');
  });
