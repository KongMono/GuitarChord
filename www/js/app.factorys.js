angular.module('app.factorys', [])
.factory('Keys', function () {

var mainKeys1 = ['A','Bb','B','C','Db','D','Eb','E','F','Gb','G','Ab'];
var mainKeys2 = ['A','A#','B','C','C#','D','D#','E','F','F#','G','G#'];
// var addOnKeys = ['','5',"sus4","sus2","add9",'6',"6add9","maj7","maj7#11","maj13",'m',"m(add9)","m6","mb6","m6add9","m7","m7b5","m(maj7)","m9","m9b5","m11","m13",'7',"7sus4","7b5"];
var addOnKeys = ["","5","sus4","sus2","add9","6","6add9","maj7","maj7#11","maj13","m","m(add9)","m6","mb6","m6add9","m7","m7b5","m(maj7)","m9","m9b5","m11","m13","7","7sus4","7b5","maj9","m9(maj7)","9","9sus4","9b5","7b9","7#9","7b5(#9)","11","7#11","13","13sus4","+","+7","+9","+7b9","+7#9","dim","dim7"]

    return {
        get: function (key1, key2, addon) {
          var tempKey = key1;
          if(key2 == 0)
            return mainKeys1[tempKey]+addOnKeys[addon];
          else
            return mainKeys2[tempKey]+addOnKeys[addon];
        }
    };
})
.factory('searchResults', function () {

    var artist = [];
    var music = [];
    // call api as search by artist name
    artist = [
        {
            'name': "ดาวอังคาร",
            'viewCount': 0,
            'key': "E",
            'convertKey': 7,
            'img': 'https://res.cloudinary.com/a0fzide/image/upload/v1458378358/iconWEM_rxpnuz.png',
            'artistTH': "ว่าน ธนกฤต",
            'artistEn': "Wan Thanakrit"
        },
        {
            'name': "ความคิด",
            'viewCount': 0,
            'key': "E",
            'convertKey': 7,
            'img': 'https://res.cloudinary.com/a0fzide/image/upload/v1458378358/iconWEM_rxpnuz.png',
            'artistTH': "แสตมป์ อภิวัฒน์",
            'artistEn': "Stamp Apiwat"
        }
    ];
    // call api as search by song name
    music = [
        {
            'name': "ดาวอังคาร",
            'viewCount': 0,
            'key': "E",
            'convertKey': 7,
            'img': 'https://res.cloudinary.com/a0fzide/image/upload/v1458378358/iconWEM_rxpnuz.png',
            'artistTH': "ว่าน ธนกฤต",
            'artistEn': "Wan Thanakrit"
        },
        {
            'name': "ความคิด",
            'viewCount': 0,
            'key': "E",
            'convertKey': 7,
            'img': 'https://res.cloudinary.com/a0fzide/image/upload/v1458378358/iconWEM_rxpnuz.png',
            'artistTH': "แสตมป์ อภิวัฒน์",
            'artistEn': "Stamp Apiwat"
        }
    ];
    // call api as search by artist with more songs
    var artistRelated = artist;
    // call api as search by poppular music
    var suggestMusic = music;

    return {
        searchArtist: function () {
            return artist;
        },
        searchMusic: function() {
            return music;
        },
        searchArtistRelated: function(){
            return artistRelated;
        },
        searchSuggest: function() {
            return suggestMusic;
        }
    };
})
.factory('autocompleteSearch', function () {

    var searchFromArtist = [];
    var searchFromMusic = [];

    // search api from artist with maximum 5 to 10
    searchFromArtist = [
        {
            'name': "ชื่อนักร้อง 1"
        },
        {
            'name': "ชื่อนักร้อง 2"
        },
        {
            'name': "ชื่อนักร้อง 3"
        },
        {
            'name': "ชื่อนักร้อง 4"
        },
        {
            'name': "ชื่อนักร้อง 5"
        }
    ];
    // search api from artist with maximum 5 to 10
    searchFromMusic = [
        {
            'name': "ชื่อเพลง 1"
        },
        {
            'name': "ชื่อเพลง 2"
        },
        {
            'name': "ชื่อเพลง 3"
        },
        {
            'name': "ชื่อเพลง 4"
        },
        {
            'name': "ชื่อเพลง 5"
        }
    ];
    return {
        searchArtist: function (keyword) {
          return searchFromArtist;
        },
        searchMusic: function (keyword){
            return searchFromMusic;
        }
    };
})
