var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var shell = require('gulp-shell');
var runSequence = require('run-sequence');
var ngAnnotate = require('gulp-ng-annotate');


var paths = {
  dist: ['./www/dist/**'],
  sass: ['./www/scss/**/*.scss'],
  js: ['./www/**/*.js']
};

//Gulp args
var args = require('yargs')
  .alias('e', 'emulate')
  .alias('s', 'serve')
  .default('build', false)
  .default('port', 9000)
  .default('strip-debug', false)
  .argv;

var emulate = args.emulate;
var serve = args.serve;

if (emulate === true) {
  emulate = 'ios';
}

var platform;
if (args.android === true) platform = 'android';
if (args.ios === true) platform = 'ios';
if (args.a === true) platform = ' ';

/*Default*/
gulp.task('default', function (done) {
  runSequence(
    [
      'sass',
      'javascript',
    ],
    emulate ? ['ionic:emulate'] : 'noop',
    serve ? ['ionic:serve'] : 'noop',
    done);
});

/*Sass*/
gulp.task('sass', function (done) {
  gulp.src(['./scss/ionic.app.scss'
    , './scss/guitarchord.scss'])
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

/* Javascript task then minified*/
gulp.task('javascript', function () {
  return gulp.src(['./www/js/*.js', './www/modules/**/*.js'])
    .pipe(concat('all.min.js'))
    .pipe(ngAnnotate())
    // .pipe(uglify())
    .pipe(gulp.dest('./www/dist/js/'));
});

/*Watch task*/
gulp.task('watch', function () {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch([paths.js, '!' + paths.dist], ['javascript']);
});

gulp.task('install', ['git-check'], function () {
  return bower.commands.install()
    .on('log', function (data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function (done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

gulp.task('ionic:serve', shell.task([
  'ionic serve'
]));

gulp.task('noop', function () { });